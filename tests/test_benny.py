from pytest import raises
from selenium.common.exceptions import NoSuchElementException


#def test_query_window_is_visible(remote_browser):
#    remote_browser.get('https://google.com')
#    query_window = remote_browser.find_element_by_name('q')
#    assert query_window.is_displayed()


def test_search_if_found(remote_browser):
    remote_browser.get('https://the-internet.herokuapp.com/context_menu')
    query_window = remote_browser.page_source
    search_text = "Right-click in the box below to see one called 'the-internet"
    assert search_text in query_window


def test_search_if_not_found(remote_browser):
    remote_browser.get('https://the-internet.herokuapp.com/context_menu')
    query_window = remote_browser.page_source
    search_text = "Alibaba"
    assert search_text not in query_window
